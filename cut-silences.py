import os
import tempfile
import ffmpeg
import pysrt

def cut_silences(audio_file, srt_file, output_extension = None, output_audio_file = None, debug=False):
    # Load subtitles
    subs = pysrt.open(srt_file)

    # Use default extension if none is provided
    if output_extension is None:
        output_extension = os.path.splitext(audio_file)[1]

    # Output file in the same directory as the input file
    if output_audio_file is None:
        output_audio_file = tempfile.mktemp(prefix="cut_", suffix="." + output_extension, dir=os.path.dirname(audio_file))

    # Create aselect filter expression
    select_expressions = [f'between(t\,{sub.start.ordinal / 1000}\,{sub.end.ordinal / 1000})' for sub in subs]
    aselect_expression = '+'.join(select_expressions)
    
    kwargs = {}

    if debug:
        print(f'aselect expression: {aselect_expression}')
        # Also print debug information from ffmpeg
        kwargs['debug'] = '1'

    # Apply aselect filter to audio file
    ffmpeg.input(audio_file, **kwargs).output(output_audio_file, af=f'aselect={aselect_expression}, asetpts=N/SR/TB').run()

    return {
        'audio': output_audio_file,
    }

if __name__ == '__main__':
    # Parse arguments
    import argparse

    parser = argparse.ArgumentParser(description='Cut silences from audio file using subtitles.')
    parser.add_argument('audio_file', help='Audio file to cut silences from.')
    parser.add_argument('srt_file', help='Subtitles file to use.')
    parser.add_argument('--output-extension', help='Output file extension. Defaults to the same as the input file.')
    parser.add_argument('--output-audio-file', help='Output audio file. Defaults to a temporary file.')
    parser.add_argument('--debug', action='store_true', help='Print debug information.')

    args = parser.parse_args()

    # Cut silences
    result = cut_silences(args.audio_file, args.srt_file, args.output_extension, args.output_audio_file, args.debug)
    print(f'New audio file: {result["audio"]}')
