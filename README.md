# Cut Silences with Subtitles

This Python script reads in an audio file and a subtitle (SRT) file, and it creates a new audio file that cuts out the silences in the original audio using the subtitle timestamps.

## Builds

You can get builds for Windows in the Relases section. Note that you must also have ffmpeg installed on your system for the script to work (can be installed next to the executable).

To build the script yourself, you can use PyInstaller:

```bash
pyinstaller --onefile cut-silences.py
```

## Requirements
- Python 3.10
- ffmpeg-python
- pysrt

Also ffmpeg must be installed.

## Installation

Clone or download the Git repository, and then install Python 3.10 or later. Then, install the dependencies using pip:

```bash
pip install -r requirements.txt
```

### Using venv
If you are using conda, you can create a new environment and install the dependencies like so:

```bash
conda create -n myenv python=3.10
conda activate myenv
pip install -r requirements.txt
```

## Usage

Once the dependencies are installed, you can run the script using the following command:

```bash
python cut-silences.py <audio_file> <srt_file> --output-extension <output_extension> --output-audio-file <output_audio_file>
```

Where:
- `<audio_file>` is the path to the audio file to cut silences from.
- `<srt_file>` is the path to the subtitles file to use.
- `<output_extension>` is the desired output file extension. It defaults to the same as the input file.
- `<output_audio_file>` is the desired output audio file. It defaults to a temporary file in the same directory as the input audio file.

For example:

```bash
python cut-silences.py my_audio.mp3 my_subtitles.srt --output-extension .wav --output-audio-file new_audio.wav
```

This will create a new audio file `new_audio.wav` from `my_audio.mp3` that cuts out the silences using the timestamps in `my_subtitles.srt`.